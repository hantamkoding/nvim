local chadtree_settings = {
  theme={
    icon_glyph_set='ascii'
  },
  keymap={
    primary={"<enter>", "l"}
  }
}
vim.api.nvim_set_var("chadtree_settings", chadtree_settings)
